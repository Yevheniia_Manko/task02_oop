package com.manko.task02;

import com.manko.task02.view.FlowerShopConsole;

public class Appl {

    public static void main(String[] args) {
        FlowerShopConsole view = new FlowerShopConsole();
        view.start();
    }
}
