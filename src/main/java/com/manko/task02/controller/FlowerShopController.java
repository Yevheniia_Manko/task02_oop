package com.manko.task02.controller;

import com.manko.task02.model.Flower;
import com.manko.task02.model.FlowerSeller;

import java.util.List;
import java.util.Map;

public class FlowerShopController {

    private FlowerSeller flowerSeller;

    public FlowerShopController() {
        flowerSeller = new FlowerSeller();
    }

    public List<Flower> getFlowerCatalog() {
        return flowerSeller.getCatalog();
    }

    public void addOrderItem(int id, int amount) {
        try {
            flowerSeller.addItem(id, amount);
        } catch (RuntimeException e){
            System.out.println(e.getMessage());
        }
    }

    public Map<Flower, Integer> getUserOrder() {
        return flowerSeller.getOrder();
    }

    public int getTotalPrice() {
        return flowerSeller.calculateTotalPrice();
    }
}
