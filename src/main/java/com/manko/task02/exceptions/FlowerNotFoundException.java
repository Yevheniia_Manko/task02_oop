package com.manko.task02.exceptions;

public class FlowerNotFoundException extends RuntimeException{

    public FlowerNotFoundException(String message) {
        super(message);
    }
}
