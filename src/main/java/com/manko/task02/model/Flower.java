package com.manko.task02.model;

public class Flower implements Comparable<Flower> {

    private int id;
    private String name;
    private String color;
    private Category category;
    private int price;

    public Flower() {

    }

    public Flower(int id, String name, String color, Category category, int price) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.category = category;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Flower " + id + " is: " + name + ", color: " + color + ", " + category
                + ", price: " + price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flower flower = (Flower) o;
        return id == flower.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public int compareTo(Flower otherFlower) {
        Integer id1 = id;
        Integer id2 = otherFlower.id;
        if (price > otherFlower.price) {
            return 1;
        } else if (price < otherFlower.price) {
            return -1;
        } else {
            return id1.compareTo(id2);
        }
    }
}
