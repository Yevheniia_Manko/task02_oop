package com.manko.task02.model;

import com.manko.task02.exceptions.FlowerNotFoundException;

import java.util.*;

public class FlowerSeller {

    private List<Flower> catalog = new ArrayList<>();
    private Map<Flower, Integer> order = new TreeMap<>();

    public FlowerSeller() {
        createCatalog();
    }

    private void createCatalog() {
        catalog.add(new Flower(1, "Rose", "Red", Category.CUT_FLOWER, 35));
        catalog.add(new Flower(2, "Rose", "White", Category.CUT_FLOWER, 33));
        catalog.add(new Flower(3, "Rose", "Yellow", Category.CUT_FLOWER, 30));
        catalog.add(new Flower(4, "Tulip", "Red", Category.CUT_FLOWER, 25));
        catalog.add(new Flower(5, "Tulip", "Yellow", Category.CUT_FLOWER, 22));
        catalog.add(new Flower(6, "Rose", "Red", Category.FLOWERPOT, 40));
        catalog.add(new Flower(7, "Rose", "Yellow", Category.FLOWERPOT, 40));
    }

    public List<Flower> getCatalog() {
        return catalog;
    }

    public Map<Flower, Integer> getOrder() {
        return order;
    }

    public int calculateTotalPrice() {
        int totalPrice = 0;
        for (Flower flower : order.keySet()) {
            int amount = order.get(flower);
            totalPrice += flower.getPrice() * amount;
        }
        return totalPrice;
    }

    public void addItem(int id, int amount) {
        Flower flower = findFlower(id);
        if (flower != null) {
            order.put(flower, amount);
        }
    }

    private Flower findFlower(int id) {
        for (int i = 0; i < catalog.size(); i++) {
            if (catalog.get(i).getId() == id) {
                return catalog.get(i);
            }
        }
        throw new FlowerNotFoundException("Invalid value for flower ID.");
    }
}
