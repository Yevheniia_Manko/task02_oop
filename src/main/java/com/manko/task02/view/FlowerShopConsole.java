package com.manko.task02.view;

import com.manko.task02.controller.FlowerShopController;
import com.manko.task02.model.Flower;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class FlowerShopConsole {

    private Scanner sc;
    private FlowerShopController controller;

    public FlowerShopConsole() {
        sc = new Scanner(System.in);
        controller = new FlowerShopController();
    }

    public void start() {
        boolean oneMore = true;
        while (oneMore) {
            int choice = mainMenu();
            switch (choice) {
                case 0:
                    System.out.println("The end.");
                    oneMore = false;
                    break;
                case 1:
                    showAllFlowers();
                    break;
                case 2:
                case 3:
                    bouquetMenu();
                    break;
                default:
                    System.out.println("You entered invalid value.");
                    System.out.println("Please enter the mainMenu item.");
                    break;
            }
        }
    }

    private int mainMenu() {
        System.out.println("---WELCOME TO OUR FLOWER SHOP---");
        System.out.println("1. Browse the flower catalog.");
        System.out.println("2. Buy a bouquet.");
        System.out.println("3. Buy a flower in a flowerpot");
        System.out.println("0. Exit");
        return readIntegerValue();
    }

    private int readIntegerValue() {
        int value;
        while (true) {
            try {
                value = Integer.parseInt(sc.nextLine());
                break;
            } catch (Exception e) {
                System.out.println("You entered invalid value.");
                System.out.println("Please try again.");
            }
        }
        return value;
    }

    private void showAllFlowers() {
        List<Flower> catalog = controller.getFlowerCatalog();
        for (Flower flower : catalog) {
            System.out.println(flower);
        }
    }

    private void bouquetMenu() {
        boolean oneMoreFlower = true;
        while (oneMoreFlower) {
            System.out.println("Enter the id of flower: ");
            int flowerId = readIntegerValue();
            System.out.println("Enter the amount: ");
            int amount = readIntegerValue();
            controller.addOrderItem(flowerId, amount);
            System.out.println("Would you like to continue? Print 'Y'");
            String userChoice = sc.nextLine();
            if (!userChoice.equalsIgnoreCase("Y")) {
                oneMoreFlower = false;
                printOrder();
            }
        }
    }

    private void printOrder() {
        Map<Flower, Integer> userOrder = controller.getUserOrder();
        System.out.println("Your order is:");
        for (Flower flower : userOrder.keySet()) {
            System.out.println(flower + " , amount = " + userOrder.get(flower));
        }
        int totalSum = controller.getTotalPrice();
        System.out.println("Total price is: " + totalSum);
    }
}
